# Climate kernels for Co-design

These kernels are intended for sharing with interested parties for co-design purposes. They originate from the Energy Exascale Earth System Model (E3SM), formerly known as Accelerated Climate Modeling for Energy (ACME).

# Contributions
We welcome your contributions in the form of well-articulated pull requests. Please contact us with any alternative suggestions. 

# Authors
Matt Norman

# Point of Contact
Please send any questions or comments to Sarat Sreepathi (sarat-at-ornl.gov).

# Acknowledgments
This research was supported by the Exascale Computing Project (17-SC-20-SC), a collaborative effort of the U.S. Department of Energy Office of Science and the National Nuclear Security Administration.   


This research was supported as part of the Energy Exascale Earth System Model (E3SM) project, funded by the U.S. Department of Energy, Office of Science, Office of Biological and Environmental Research.  


This research used resources of the Oak Ridge Leadership Computing Facility at the Oak Ridge National Laboratory, which is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC05-00OR22725.  
